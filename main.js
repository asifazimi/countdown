// This bunch of codes has been written by me

const months = [
  "january",
  "february",
  "march",
  "april",
  "may",
  "june",
  "july",
  "august",
  "september",
  "october",
  "november",
  "december",
];

const days = [
  "sunday",
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday",
];

let deadline = document.querySelector(".deadline");
const giveaway = document.querySelector(".giveaway");
const items = document.querySelectorAll(".deadline-format h1");

let futureTime = new Date(2021, 9, 23, 1, 0, 0);

const year = futureTime.getFullYear();
const hour = futureTime.getHours();
const minute = futureTime.getMinutes();
const month = futureTime.getMonth();
const day = futureTime.getDay();
const date = futureTime.getDate();

giveaway.textContent = `Giveaway ends on ${days[day]}, ${months[month]} ${date}, ${year} ${hour}:${minute}am`;

function getRemainingTime() {
  const today = new Date();
  const time = futureTime - today;

  const oneDay = 24 * 60 * 60 * 1000;
  const oneHour = 60 * 60 * 1000;
  const oneMinute = 60 * 1000;
  const oneSecond = 1000;
  const day = Math.floor(time / oneDay);
  const hour = Math.floor((time % oneDay) / oneHour);
  const minute = Math.floor((time % oneHour) / oneMinute);
  const second = Math.floor((time % oneMinute) / oneSecond);

  function format(item) {
    if (item < 10) {
      return (item = `0${item}`);
    }
    return item;
  }

  const values = [day, hour, minute, second];
  items.forEach((item, index) => {
    item.innerHTML = format(values[index]);
  });

  if (time < 0) {
    clearInterval(interval);
    deadline.innerHTML = `<h4>Sorry, Time has expired.</h4>`;
  }
}

let interval = setInterval(getRemainingTime, 1000);

getRemainingTime();
